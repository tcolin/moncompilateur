# COLIN Theo COMPILER

**Fonctions ajouté :**
**Ajout de la Boucle FOR TO/DOWNTO DO** -> opérationnel

**Ajout de CASE** -> Ne marche qu'avec les entiers. 
Problème de segmentation rencontré quand je lance ./test, en utilisant ddd test, je remarque que ça marche et que le programme fait ce que je voulais qu'il fasse. 
Pour les caractères, la comparaison entre une constante et une variable ne fonctionne pas ex : DISPLAY (a=='a') me mettra FALSE même si a:='a' 

**Commande Framagit**
A simple compiler.
From : Pascal-like imperative LL(k) langage
To : 64 bit 80x86 assembly langage (AT&T)

**Download the repository :**

> git clone git@framagit.org:tcolin/moncompilateur.git

**Build the compiler and test it :**

> make test

**Have a look at the output :**

> gedit test.s

**Debug the executable :**

> ddd ./test

**Commit the new version :**

> git commit -a -m "What's new..."

**Send to your framagit :**

> git push -u origin master

**Get from your framagit :**

> git pull -u origin master

**This version Can handle :**

// Program := [DeclarationPart] StatementPart
// DeclarationPart := "[" Identifier {"," Identifier} "]"
// StatementPart := Statement {";" Statement} "."
// Statement := AssignementStatement
// AssignementStatement := Identifier ":=" Expression

// Expression := SimpleExpression [RelationalOperator SimpleExpression]
// SimpleExpression := Term {AdditiveOperator Term}
// Term := Factor {MultiplicativeOperator Factor}
// Factor := Number | Letter | "(" Expression ")"| "!" Factor
// Number := Digit{Digit}
// Identifier := Letter {(Letter|Digit)}

// AdditiveOperator := "+" | "-" | "||"
// MultiplicativeOperator := "*" | "/" | "%" | "&&"
// RelationalOperator := "==" | "!=" | "<" | ">" | "<=" | ">="  
// Digit := "0"|"1"|"2"|"3"|"4"|"5"|"6"|"7"|"8"|"9"
// Letter := "a"|...|"z"

// ForStatement := "FOR" AssignementStatement "TO|DOWNTO" Expression "DO" Statement
// IfStatement := "IF" Expression "THEN" Statement [ "ELSE" Statement ]
// <case label list> ::= <constant>
// <case list element> ::= <case label list> {',' case label list} : <statement> | <empty>)
// <case statement> ::= case <expression> of <case list element> {; <case list element> } 'ELSE' <Statement> end
// empty::=
// WhileStatement := "WHILE" Expression DO Statement
// BlockStatement := "BEGIN" Statement { ";" Statement } "END"

