			# This code was produced by COLIN Theo
.data
FormatString1:	.string "%llu"	# used by printf to display 64-bit unsigned integers
FormatString2:	.string "%lf"	# used by printf to display 64-bit floating point numbers
FormatString3:	.string "%c"	# used by printf to display a 8-bit single character
TrueString:	.string "TRUE"	# used by printf to display the boolean value TRUE
FalseString:	.string "FALSE"	# used by printf to display the boolean value FALSE
a:	.quad 0
b:	.quad 0
i:	.quad 0
j:	.quad 0
	.align 8
	.text		# The following lines contain the program
	.globl main	# The main function must be visible from outside
main:			# The main function body :
	movq %rsp, %rbp	# Save the position of the stack's top
	push $10
	pop a
	push $1
	pop b
	movq $0, %rax
	movb $'b',%al
	push %rax	# push a 64-bit version of 'b'
	pop %rsi			# get character in the 8 lowest bits of %si
	movq $FormatString3, %rdi	# "%c\n"
	movl	$0, %eax
	call	printf@PLT
	movq $0, %rax
	movb $'=',%al
	push %rax	# push a 64-bit version of '='
	pop %rsi			# get character in the 8 lowest bits of %si
	movq $FormatString3, %rdi	# "%c\n"
	movl	$0, %eax
	call	printf@PLT
	push b
	pop %rdx	# La valeur devant être affiché
	movq $FormatString1, %rsi	# "%llu\n"
	movl	$1, %edi
	movl	$0, %eax
	call	__printf_chk@PLT
	movq $0, %rax
	movb $'\n',%al
	push %rax	# push a 64-bit version of '\n'
	pop %rsi			# get character in the 8 lowest bits of %si
	movq $FormatString3, %rdi	# "%c\n"
	movl	$0, %eax
	call	printf@PLT
	movq $0, %rax
	movb $'\n',%al
	push %rax	# push a 64-bit version of '\n'
	pop %rsi			# get character in the 8 lowest bits of %si
	movq $FormatString3, %rdi	# "%c\n"
	movl	$0, %eax
	call	printf@PLT
Case6:
	push a
DebutCase6:
	push $6
	pop %rax
	push a
	pop %rcx
	cmpq %rax, %rcx
	je CaseTrue6
	jne CaseFalse6
CaseTrue6:	# True
	push $8
	pop b
	jmp FinCase6
CaseFalse6:
DebutCase7:
	push $4
	pop %rax
	push a
	pop %rcx
	cmpq %rax, %rcx
	je CaseTrue7
	push $8
	pop %rax
	push a
	pop %rcx
	cmpq %rax, %rcx
	je CaseTrue7
	push $10
	pop %rax
	push a
	pop %rcx
	cmpq %rax, %rcx
	je CaseTrue7
	push $5
	pop %rax
	push a
	pop %rcx
	cmpq %rax, %rcx
	je CaseTrue7
	jne CaseFalse7
CaseTrue7:	# True
ForInit9:
	push $3
	pop j
For9:
	push $1
	pop %rax
	push j
	pop %rcx
	cmpq %rcx, %rax
	ja FinFor9
BEGIN10:
	movq $0, %rax
	movb $'a',%al
	push %rax	# push a 64-bit version of 'a'
	pop %rsi			# get character in the 8 lowest bits of %si
	movq $FormatString3, %rdi	# "%c\n"
	movl	$0, %eax
	call	printf@PLT
	movq $0, %rax
	movb $'=',%al
	push %rax	# push a 64-bit version of '='
	pop %rsi			# get character in the 8 lowest bits of %si
	movq $FormatString3, %rdi	# "%c\n"
	movl	$0, %eax
	call	printf@PLT
	push a
	pop %rdx	# La valeur devant être affiché
	movq $FormatString1, %rsi	# "%llu\n"
	movl	$1, %edi
	movl	$0, %eax
	call	__printf_chk@PLT
	movq $0, %rax
	movb $'\n',%al
	push %rax	# push a 64-bit version of '\n'
	pop %rsi			# get character in the 8 lowest bits of %si
	movq $FormatString3, %rdi	# "%c\n"
	movl	$0, %eax
	call	printf@PLT
	push a
	push $1
	pop %rbx
	pop %rax
	subq	%rbx, %rax	# SUB
	push %rax
	pop a
	push j
	pop %rax
	subq $1, %rax
	push %rax
	pop j
	jmp For9
FinFor9:
	jmp FinCase6
CaseFalse7:
DebutCase14:
	push $8
	pop %rax
	push a
	pop %rcx
	cmpq %rax, %rcx
	je CaseTrue14
	jne CaseFalse14
CaseTrue14:	# True
	push $10
	pop b
	jmp FinCase6
CaseFalse14:
ELSE :
	push $48
	pop b
FinCase6:
	movq $0, %rax
	movb $'\n',%al
	push %rax	# push a 64-bit version of '\n'
	pop %rsi			# get character in the 8 lowest bits of %si
	movq $FormatString3, %rdi	# "%c\n"
	movl	$0, %eax
	call	printf@PLT
ForInit17:
	push $1
	pop i
For17:
	push $3
	pop %rax
	push i
	pop %rcx
	cmpq %rcx, %rax
	jb FinFor17
BEGIN18:
ForInit19:
	push $3
	pop j
For19:
	push $1
	pop %rax
	push j
	pop %rcx
	cmpq %rcx, %rax
	ja FinFor19
BEGIN20:
	movq $0, %rax
	movb $'a',%al
	push %rax	# push a 64-bit version of 'a'
	pop %rsi			# get character in the 8 lowest bits of %si
	movq $FormatString3, %rdi	# "%c\n"
	movl	$0, %eax
	call	printf@PLT
	movq $0, %rax
	movb $'=',%al
	push %rax	# push a 64-bit version of '='
	pop %rsi			# get character in the 8 lowest bits of %si
	movq $FormatString3, %rdi	# "%c\n"
	movl	$0, %eax
	call	printf@PLT
	push a
	pop %rdx	# La valeur devant être affiché
	movq $FormatString1, %rsi	# "%llu\n"
	movl	$1, %edi
	movl	$0, %eax
	call	__printf_chk@PLT
	movq $0, %rax
	movb $'\n',%al
	push %rax	# push a 64-bit version of '\n'
	pop %rsi			# get character in the 8 lowest bits of %si
	movq $FormatString3, %rdi	# "%c\n"
	movl	$0, %eax
	call	printf@PLT
	push a
	push $1
	pop %rbx
	pop %rax
	addq	%rbx, %rax	# ADD
	push %rax
	pop a
	push j
	pop %rax
	subq $1, %rax
	push %rax
	pop j
	jmp For19
FinFor19:
	movq $0, %rax
	movb $'\n',%al
	push %rax	# push a 64-bit version of '\n'
	pop %rsi			# get character in the 8 lowest bits of %si
	movq $FormatString3, %rdi	# "%c\n"
	movl	$0, %eax
	call	printf@PLT
	movq $0, %rax
	movb $'b',%al
	push %rax	# push a 64-bit version of 'b'
	pop %rsi			# get character in the 8 lowest bits of %si
	movq $FormatString3, %rdi	# "%c\n"
	movl	$0, %eax
	call	printf@PLT
	movq $0, %rax
	movb $'=',%al
	push %rax	# push a 64-bit version of '='
	pop %rsi			# get character in the 8 lowest bits of %si
	movq $FormatString3, %rdi	# "%c\n"
	movl	$0, %eax
	call	printf@PLT
	push b
	pop %rdx	# La valeur devant être affiché
	movq $FormatString1, %rsi	# "%llu\n"
	movl	$1, %edi
	movl	$0, %eax
	call	__printf_chk@PLT
	movq $0, %rax
	movb $'\n',%al
	push %rax	# push a 64-bit version of '\n'
	pop %rsi			# get character in the 8 lowest bits of %si
	movq $FormatString3, %rdi	# "%c\n"
	movl	$0, %eax
	call	printf@PLT
	push b
	push $1
	pop %rbx
	pop %rax
	addq	%rbx, %rax	# ADD
	push %rax
	pop b
	push i
	pop %rax
	addq $1, %rax
	push %rax
	pop i
	jmp For17
FinFor17:
	movq $0, %rax
	movb $'\n',%al
	push %rax	# push a 64-bit version of '\n'
	pop %rsi			# get character in the 8 lowest bits of %si
	movq $FormatString3, %rdi	# "%c\n"
	movl	$0, %eax
	call	printf@PLT
	movq $0, %rax
	movb $'b',%al
	push %rax	# push a 64-bit version of 'b'
	pop %rsi			# get character in the 8 lowest bits of %si
	movq $FormatString3, %rdi	# "%c\n"
	movl	$0, %eax
	call	printf@PLT
	movq $0, %rax
	movb $'=',%al
	push %rax	# push a 64-bit version of '='
	pop %rsi			# get character in the 8 lowest bits of %si
	movq $FormatString3, %rdi	# "%c\n"
	movl	$0, %eax
	call	printf@PLT
	push b
	pop %rdx	# La valeur devant être affiché
	movq $FormatString1, %rsi	# "%llu\n"
	movl	$1, %edi
	movl	$0, %eax
	call	__printf_chk@PLT
	movq $0, %rax
	movb $'\n',%al
	push %rax	# push a 64-bit version of '\n'
	pop %rsi			# get character in the 8 lowest bits of %si
	movq $FormatString3, %rdi	# "%c\n"
	movl	$0, %eax
	call	printf@PLT
	movq $0, %rax
	movb $'a',%al
	push %rax	# push a 64-bit version of 'a'
	pop %rsi			# get character in the 8 lowest bits of %si
	movq $FormatString3, %rdi	# "%c\n"
	movl	$0, %eax
	call	printf@PLT
	movq $0, %rax
	movb $'=',%al
	push %rax	# push a 64-bit version of '='
	pop %rsi			# get character in the 8 lowest bits of %si
	movq $FormatString3, %rdi	# "%c\n"
	movl	$0, %eax
	call	printf@PLT
	push a
	pop %rdx	# La valeur devant être affiché
	movq $FormatString1, %rsi	# "%llu\n"
	movl	$1, %edi
	movl	$0, %eax
	call	__printf_chk@PLT
	movq $0, %rax
	movb $'\n',%al
	push %rax	# push a 64-bit version of '\n'
	pop %rsi			# get character in the 8 lowest bits of %si
	movq $FormatString3, %rdi	# "%c\n"
	movl	$0, %eax
	call	printf@PLT
	movq %rbp, %rsp		# Restore the position of the stack's top
	ret			# Return from main function
