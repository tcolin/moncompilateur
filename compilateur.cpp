//  A compiler from a very simple Pascal-like structured language LL(k)
//  to 64-bit 80x86 Assembly langage
//  Copyright (C) 2019 Pierre Jourlin
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//  
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//  
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <https://www.gnu.org/licenses/>.

// Build with "make compilateur"


#include <string>
#include <iostream>
#include <cstdlib>
#include <set>
#include <map>
#include <FlexLexer.h>
#include "tokeniser.h"
#include <cstring>

using namespace std;

enum OPREL {EQU, DIFF, INF, SUP, INFE, SUPE, WTFR};
enum OPADD {ADD, SUB, OR, WTFA};
enum OPMUL {MUL, DIV, MOD, AND ,WTFM};
enum TYPES {INTEGER, BOOL, DOUBLE, CHAR};
TOKEN current;				// Current token


FlexLexer* lexer = new yyFlexLexer; // This is the flex tokeniser
// tokens can be read using lexer->yylex()
// lexer->yylex() returns the type of the lexicon entry (see enum TOKEN in tokeniser.h)
// and lexer->YYText() returns the lexicon entry as a string

	
map<string, enum TYPES> DeclaredVariables;
unsigned long long TagNumber=0;
unsigned long long TagNumber2=1;

bool IsDeclared(const char *id){
	return DeclaredVariables.find(id)!=DeclaredVariables.end();
}


void Error(string s){
	cerr << "Ligne n°"<<lexer->lineno()<<", lu : '"<<lexer->YYText()<<"'("<<current<<"), mais ";
	cerr<< s << endl;
	exit(-1);
}

// Program := [DeclarationPart] StatementPart
// DeclarationPart := "[" Letter {"," Letter} "]"
// StatementPart := Statement {";" Statement} "."
// Statement := AssignementStatement
// AssignementStatement := Letter "=" Expression

// Expression := SimpleExpression [RelationalOperator SimpleExpression]
// SimpleExpression := Term {AdditiveOperator Term}
// Term := Factor {MultiplicativeOperator Factor}
// Factor := Number | Letter | "(" Expression ")"| "!" Factor
// Number := Digit{Digit}

// AdditiveOperator := "+" | "-" | "||"
// MultiplicativeOperator := "*" | "/" | "%" | "&&"
// RelationalOperator := "==" | "!=" | "<" | ">" | "<=" | ">="  
// Digit := "0"|"1"|"2"|"3"|"4"|"5"|"6"|"7"|"8"|"9"
// Letter := "a"|...|"z"
enum TYPES Expression(void);
enum TYPES Constant(void);
void Statement(void);
void AssignementStatement(void);
enum TYPES CaseLabelList(void);
enum TYPES CaseListElement(string var, bool &stat);

// empty::=
void empty(void) {
	
}

// <case statement> ::= case <expression> of <case list element> {; <case list element> } 'ELSE' <Statement> end
void CaseStatement(void) {
	TYPES type1, type2;
	string var;
	bool stat=false;
	unsigned long long tagN = ++TagNumber;
	TagNumber2=TagNumber;
	if(current==KEYWORD && !strcmp(lexer->YYText(),"CASE"))
	{
		current=(TOKEN) lexer->yylex();
		var = lexer->YYText();
	}
	else Error("Mot clé 'CASE' attendu");
	cout << "Case" << tagN << ":" << endl;
	type1 = Expression();
	if(current == KEYWORD && !strcmp(lexer->YYText(),"OF"))
	{
		current=(TOKEN) lexer->yylex();
	}
	else Error("Mot clé 'OF' attendu");
	type2=CaseListElement(var,stat);
	if (type2!=type1){
		Error("Le Case et la constante sont de types différents");
	}

	while(current == SEMICOLON || (current == KEYWORD && !strcmp(lexer->YYText(),"ELSE"))) {
		current=(TOKEN) lexer->yylex();
		if(current == KEYWORD && !strcmp(lexer->YYText(),"ELSE")) {
			current=(TOKEN) lexer->yylex();
			cout << "ELSE :" << endl;
			Statement();
		}
		else {
			type2=CaseListElement(var, stat);
			if (type2!=type1){
			Error("Le Case et la constante sont de types différents");
			}
		}
	}
	if(current == KEYWORD && !strcmp(lexer->YYText(),"END")) {
		current=(TOKEN) lexer->yylex();
	}
	else Error("Mot clé 'END' attendu #CASE");
	cout << "FinCase" << tagN << ":" << endl;
	TagNumber2++;
}

// <case list element> ::= <case label list> {',' case label list} : <statement> | <empty>)
enum TYPES CaseListElement(string var, bool &statement) {
		enum TYPES type;
		unsigned long long tag = TagNumber2;
		unsigned long long tagN = TagNumber;
		TagNumber++;
		statement = false;
		cout << "DebutCase" << tagN << ":" << endl;
		type=CaseLabelList();
		if(type!=DOUBLE) {
			cout << "\tpop %rax" << endl;
			cout << "\tpush " << var << endl;
			cout << "\tpop %rcx" << endl;
			cout << "\tcmpq %rax, %rcx" << endl;
			cout << "\tje CaseTrue" << tagN << endl;
		//	cout << "\tjne CaseFalse" << tagN << endl;
		}
		else {
			cout << "\tpush " << var << endl;
			cout << "\tfldl (%rsp)\t" << endl;
			//cout << "\tfldl " << var << "\t" << endl;
			cout << "\tfldl 8(%rsp)\t" << endl;
			//cout << "\taddq $16, %rsp\t" << endl;
			cout << "\tfcomip %st(1)\t" << endl;
			cout << "\tfaddp %st(1)\t" << endl;
			cout << "\tje CaseTrue" << tagN << endl;
			//cout << "\tjne CaseFalse" << tagN << endl;
		}
		while(current==COMMA) {
			current = (TOKEN) lexer->yylex();
			type=CaseLabelList();
			if(type!=DOUBLE) {
				cout << "\tpop %rax" << endl;
				cout << "\tpush " << var << endl;
				cout << "\tpop %rcx" << endl;
				cout << "\tcmpq %rax, %rcx" << endl;
				cout << "\tje CaseTrue" << tagN << endl;
			}
			else {
				cout << "\tpush " << var << endl;
				cout << "\tfldl (%rsp)\t" << endl;
				//cout << "\tfldl " << var << "\t" << endl;
				cout << "\tfldl 8(%rsp)\t" << endl;
				cout << "\taddq $16, %rsp\t" << endl;
				cout << "\tfcomip %st(1)\t" << endl;
				cout << "\tfaddp %st(1)\t" << endl;
				cout << "\tje CaseTrue" << tagN << endl;
				//cout << "\tjne CaseFalse" << tagN << endl;
			}
			
		}
		cout << "\tjne CaseFalse" << tagN << endl;
		
		if(current == COLON) {
			current=(TOKEN) lexer->yylex();
		}
		else Error("':' attendu");
		cout << "CaseTrue"<<tagN<<":\t# True"<<endl;		
		statement=true;
		
		if(current==ID || current==KEYWORD) {
			Statement();
			//current=(TOKEN) lexer->yylex();
			cout <<"\tjmp FinCase" << tag << endl;
		}
		else if(statement) {
			empty();
			cout <<"\tjmp FinCase" << tag << endl;
		}	
		cout << "CaseFalse" << tagN << ":" <<endl;
		return type;
}

// <case label list> ::= <constant>
enum TYPES CaseLabelList(void) {
	enum TYPES type;
	type=Constant();
	return type;

}

// IfStatement := "IF" Expression "THEN" Statement [ "ELSE" Statement ]
void IfStatement(void)
{
	TYPES type;
	unsigned long long tagN = ++TagNumber;
	if(current==KEYWORD && !strcmp(lexer->YYText(),"IF"))
	{
		current=(TOKEN) lexer->yylex();
	}
	else Error("Mot clé 'IF' attendu");
	cout << "If" << tagN << ":" << endl;
	type=Expression();
	if(type!=BOOL) {
		Error("type boolean attendu dans l'expression");
	}
	cout << "\tpop %rax" << endl;
	cout << "\tcmpq $0, %rax" << endl;
	cout << "\tje Else" << tagN << endl;
	if(current == KEYWORD && !strcmp(lexer->YYText(),"THEN"))
	{
		current=(TOKEN) lexer->yylex();
	}
	else Error("Mot clé 'THEN' attendu");
	Statement();
	cout << "\tjmp FinIF" << tagN << endl;
	cout << "Else" << tagN << ":" << endl;
	if(current==KEYWORD && !strcmp(lexer->YYText(),"ELSE"))
	{
		current=(TOKEN) lexer->yylex();
		Statement();
	}
	cout << "FinIF" << tagN << ":" << endl;
}

// WhileStatement := "WHILE" Expression DO Statement
void WhileStatement(void)
{
	TYPES type;
	unsigned long long tagN = ++TagNumber;
	if(current==KEYWORD && !strcmp(lexer->YYText(),"WHILE"))
	{
		current=(TOKEN) lexer->yylex();
	}
	else Error("Mot clé 'WHILE' attendu");
	cout << "While" << tagN << ":" << endl;
	type=Expression();
	if(type!=BOOL) {
		Error("type bool attendu dans l'expression");
	}
	cout << "\tpop %rax" << endl;
	cout << "\tcmpq $0, %rax" << endl;
	cout << "\tje FinWHILE" << tagN << endl;
	if(current == KEYWORD || !strcmp(lexer->YYText(), "DO"))
	{
		current=(TOKEN) lexer->yylex();
	}
	else
	{
		Error("Mot clé 'DO' attendu");
	}
	Statement();
	cout <<"\tjmp While" << tagN << endl;
	cout << "FinWHILE" << tagN << ":" << endl;
}

// ForStatement := "FOR" AssignementStatement "TO|DOWNTO" Expression "DO" Statement
void ForStatement(void)
{
	string var;
	unsigned long long tagN = ++TagNumber;
	if(current==KEYWORD && !strcmp(lexer->YYText(),"FOR"))
	{
		current=(TOKEN) lexer->yylex();
		var = lexer->YYText();
	}
	else Error("Mot clé 'FOR' attendu");
	cout << "ForInit" << tagN << ":" << endl;
	AssignementStatement();
	
	cout << "For" << tagN << ":" << endl;
	//cout << "\tpop " << var << endl;
	//cout << "\tmovq %rax, %rcx" << endl;
	if(current == KEYWORD && !strcmp(lexer->YYText(), "TO")) {
		current=(TOKEN) lexer->yylex();
		Expression();
		cout << "\tpop %rax" << endl;
		//cout << "\tpop %rcx" << endl;
		cout << "\tpush " << var << endl;
		cout << "\tpop %rcx" << endl;
		cout << "\tcmpq %rcx, %rax" << endl;
		cout << "\tjb FinFor" << tagN << endl;
		if(current == KEYWORD && !strcmp(lexer->YYText(), "DO")) {
			current=(TOKEN) lexer->yylex();
		}
		else {
		Error("Mot clé 'DO' attendu");
		}
		Statement();
		cout << "\tpush " << var << endl;
		cout << "\tpop %rax" << endl;
		cout << "\taddq $1, %rax" <<endl;
		cout << "\tpush %rax" << endl;
		cout << "\tpop " << var << endl;
		cout << "\tjmp For" << tagN << endl;
		cout << "FinFor" << tagN << ":" << endl;
	}
	else if(current == KEYWORD && !strcmp(lexer->YYText(), "DOWNTO")) {
		current=(TOKEN) lexer->yylex();
		Expression();
		cout << "\tpop %rax" << endl;
		//cout << "\tpop %rcx" << endl;
		cout << "\tpush " << var << endl;
		cout << "\tpop %rcx" << endl;
		cout << "\tcmpq %rcx, %rax" << endl;
		cout << "\tja FinFor" << tagN << endl;
		if(current == KEYWORD && !strcmp(lexer->YYText(), "DO"))
		{
			current=(TOKEN) lexer->yylex();
		}
		else
		{
			Error("Mot clé 'DO' attendu");
		}
		Statement();
		cout << "\tpush " << var << endl;
		cout << "\tpop %rax" << endl;
		cout << "\tsubq $1, %rax" <<endl;
		cout << "\tpush %rax" << endl;
		cout << "\tpop " << var << endl;
		cout << "\tjmp For" << tagN << endl;
		cout << "FinFor" << tagN << ":" << endl;
	}
	else {
		Error("Mot clé 'TO' ou 'DOWNTO' attendu");
	}
	
}

// BlockStatement := "BEGIN" Statement { ";" Statement } "END"
void BlockStatement(void)
{
	unsigned long long tagN = ++TagNumber;
	if(current==KEYWORD && !strcmp(lexer->YYText(),"BEGIN"))
	{
		current=(TOKEN) lexer->yylex();
	}
	else Error("Mot clé 'BEGIN' attendu");
	cout << "BEGIN" << tagN << ":" << endl;
	Statement();
	while(current == SEMICOLON)
	{
		current=(TOKEN) lexer->yylex();
		Statement();
	}
	if(current==KEYWORD && !strcmp(lexer->YYText(),"END"))
	{
		current=(TOKEN) lexer->yylex();
	}
	else Error("Mot clé 'END' attendu");
}

void DisplayAssignement(void) {
	TYPES type;
	unsigned long long tag = ++TagNumber;
	current=(TOKEN) lexer->yylex();
	type = Expression();
	if(type==INTEGER) {
		cout << "\tpop %rdx\t# La valeur devant être affiché" << endl;
		cout << "\tmovq $FormatString1, %rsi\t# \"%llu\\n\"" << endl;
		cout << "\tmovl	$1, %edi"<<endl;
		cout << "\tmovl	$0, %eax"<<endl;
		cout << "\tcall	__printf_chk@PLT"<<endl;
	}
	else if(type==BOOL) {
		cout << "\tpop %rdx\t# Zero : False, non-zero : true"<<endl;
		cout << "\tcmpq $0, %rdx"<<endl;
		cout << "\tje False"<<tag<<endl;
		cout << "\tmovq $TrueString, %rdi\t# \"TRUE\\n\""<<endl;
		cout << "\tjmp Next"<<tag<<endl;
		cout << "False"<<tag<<":"<<endl;
		cout << "\tmovq $FalseString, %rdi\t# \"FALSE\\n\""<<endl;
		cout << "Next"<<tag<<":"<<endl;
		cout << "\tcall	puts@PLT"<<endl;
	}
	else if(type==DOUBLE) {
		cout << "\tmovsd	(%rsp), %xmm0\t\t# &stack top -> %xmm0"<<endl;
		cout << "\tsubq	$16, %rsp\t\t# allocation for 3 additional doubles"<<endl;
		cout << "\tmovsd %xmm0, 8(%rsp)"<<endl;
		cout << "\tmovq $FormatString2, %rdi\t# \"%lf\\n\""<<endl;
		cout << "\tmovq	$1, %rax"<<endl;
		cout << "\tcall	printf"<<endl;
		cout << "nop"<<endl;
		cout << "\taddq $24, %rsp\t\t\t# pop nothing"<<endl;
	}
	else if(type==CHAR) {
		cout<<"\tpop %rsi\t\t\t# get character in the 8 lowest bits of %si"<<endl;
		cout << "\tmovq $FormatString3, %rdi\t# \"%c\\n\""<<endl;
		cout << "\tmovl	$0, %eax"<<endl;
		cout << "\tcall	printf@PLT"<<endl;
	}
	else {
		Error("DISPLAY ne fonctione pas pour ce type de données.");
	}
}

		
enum TYPES Identifier(void) {
	enum TYPES type;
	if(!IsDeclared(lexer->YYText())) {
		cerr << "Erreur : Variable '" << lexer->YYText() << "'non déclarée" << endl;
	}
	type = DeclaredVariables[lexer->YYText()];
	cout << "\tpush "<<lexer->YYText()<<endl;
	current=(TOKEN) lexer->yylex();
	return type;
}

// Différencie les entiers et les flottants
enum TYPES Number(void) {
	double f;
	unsigned int *i;
	string nombre = lexer->YYText();
	if(nombre.find(".")!=string::npos) { // Pour les nombres flottants
		f=atof(lexer->YYText());
		i=(unsigned int *) &f;
		cout <<"\tsubq $8,%rsp\t\t\t# allocate 8 bytes on stack's top"<<endl;
		cout <<"\tmovl	$"<<*i<<", (%rsp)\t# Conversion of "<<f<<" (32 bit high part)"<<endl;
		cout <<"\tmovl	$"<<*(i+1)<<", 4(%rsp)\t# Conversion of "<<f<<" (32 bit low part)"<<endl;

		current=(TOKEN) lexer->yylex();
		return DOUBLE;
	}
	else { // Pour les nombres entiers
		cout <<"\tpush $"<<atoi(lexer->YYText())<<endl;
		current=(TOKEN) lexer->yylex();
		return INTEGER;
	}
}

//Pour les caractères
enum TYPES ConstChar(void) {
	cout<<"\tmovq $0, %rax"<<endl;
	cout<<"\tmovb $"<<lexer->YYText()<<",%al"<<endl;
	cout<<"\tpush %rax\t# push a 64-bit version of "<<lexer->YYText()<<endl;
	current=(TOKEN) lexer->yylex();
	return CHAR;
}

enum TYPES Factor(void) {
	TYPES type;
	if(current==RPARENT){
		current=(TOKEN) lexer->yylex();
		type=Expression();
		if(current!=LPARENT)
			Error("')' était attendu");		// ")" expected
		else
			current=(TOKEN) lexer->yylex();
	}
	else {
		if (current==NUMBER) {
			type=Number();
		}
	    else if(current==ID) {
			type=Identifier();
		}
		else if(current==CHARCONST) {
			type=ConstChar();
		}
		else {
			Error("'(' ou chiffre ou lettre attendue");
		}
	}
	return type;
}

// MultiplicativeOperator := "*" | "/" | "%" | "&&"
OPMUL MultiplicativeOperator(void){
	OPMUL opmul;
	if(strcmp(lexer->YYText(),"*")==0)
		opmul=MUL;
	else if(strcmp(lexer->YYText(),"/")==0)
		opmul=DIV;
	else if(strcmp(lexer->YYText(),"%")==0)
		opmul=MOD;
	else if(strcmp(lexer->YYText(),"&&")==0)
		opmul=AND;
	else opmul=WTFM;
	current=(TOKEN) lexer->yylex();
	return opmul;
}

// Term := Factor {MultiplicativeOperator Factor}
enum TYPES Term(void){
	TYPES type1, type2;
	OPMUL mulop;
	type1=Factor();
	while(current==MULOP){
		mulop=MultiplicativeOperator();		// Save operator in local variable
		type2=Factor();
		if(type1!=type2) {
			Error("types différent dans l'expression");
		}
		switch(mulop){
			case AND:
				if(type2!=BOOL) {
					Error("type boolean attendu");
				}
				cout << "\tpop %rbx"<<endl;
				cout << "\tpop %rax"<<endl;
				cout << "\tmulq	%rbx"<<endl;	// a * b -> %rdx:%rax
				cout << "\tpush %rax\t# AND"<<endl;	// store result
				break;
			case MUL:
				if(type2!=INTEGER && type2!=DOUBLE) {
					Error("type INTEGER ou DOUBLE attendu");
				}
				if(type2==INTEGER) {
					cout << "\tpop %rbx"<<endl;
					cout << "\tpop %rax"<<endl;
					cout << "\tmulq	%rbx"<<endl;	// a * b -> %rdx:%rax
					cout << "\tpush %rax\t# MUL"<<endl;	// store result
				}
				else {
					cout << "\tfldl 8(%rsp)\t" << endl;
					cout << "\tfldl (%rsp)\t" << endl;
					cout << "\tfmulp	%st(0), %st(1)\t" << endl;
					cout << "\tfstpl 8(%rsp)" << endl;
					cout << "\taddq $8, %rsp\t" << endl;
				}
				break;
			case DIV:
				if(type2!=INTEGER && type2!=DOUBLE) {
					Error("type INTEGER ou DOUBLE attendu");
				}
				if(type2==INTEGER) {
					cout << "\tpop %rbx"<<endl;
					cout << "\tpop %rax"<<endl;
					cout << "\tmovq	$0, %rdx"<<endl;	// a * b -> %rdx:%rax
					cout << "\tpush %rax\t# DIV"<<endl;	// store result
				}
				else {
					cout << "\tfldl (%rsp)\t" << endl;
					cout << "\tfldl 8(%rsp)\t" << endl;
					cout << "\tfdivp	%st(0), %st(1)\t" << endl;
					cout << "\tfstpl 8(%rsp)" << endl;
					cout << "\taddq $8, %rsp\t" << endl;
				}
				break;
			case MOD:
				if(type2!=INTEGER) {
					Error("type INTEGER attendu");
				}
				cout << "\tmovq $0, %rdx"<<endl; 	// Higher part of numerator  
				cout << "\tdiv %rbx"<<endl;			// remainder goes to %rdx
				cout << "\tpush %rdx\t# MOD"<<endl;		// store result
				break;
			default:
				Error("opérateur multiplicatif attendu");
		}
	}
	return type1;
}

// AdditiveOperator := "+" | "-" | "||"
OPADD AdditiveOperator(void){
	OPADD opadd;
	if(strcmp(lexer->YYText(),"+")==0)
		opadd=ADD;
	else if(strcmp(lexer->YYText(),"-")==0)
		opadd=SUB;
	else if(strcmp(lexer->YYText(),"||")==0)
		opadd=OR;
	else opadd=WTFA;
	current=(TOKEN) lexer->yylex();
	return opadd;
}

// SimpleExpression := Term {AdditiveOperator Term}
enum TYPES SimpleExpression(void){
	TYPES type1, type2;
	OPADD adop;
	type1=Term();
	while(current==ADDOP){
		adop=AdditiveOperator();		// Save operator in local variable
		type2=Term();
		if(type2!=type1) {
			Error("types différent dans l'expression");
		}
		switch(adop){
			case OR:
				if(type2!=BOOL) {
					Error("type boolean OR attendu");
				}
				cout << "\tpop %rbx"<<endl;
				cout << "\tpop %rax"<<endl;
				cout << "\torq	%rbx, %rax\t# OR"<<endl;// operand1 OR operand2
				cout << "\tpush %rax" << endl;
				break;			
			case ADD:
				if(type2!=INTEGER && type2!=DOUBLE) {
					Error("type INTEGER ou DOUBLE attendu");
				}
				if(type2==INTEGER) {
					cout << "\tpop %rbx"<<endl;
					cout << "\tpop %rax"<<endl;
					cout << "\taddq	%rbx, %rax\t# ADD"<<endl;	// add both operands
					cout << "\tpush %rax" << endl;
				}
				else {
					cout<<"\tfldl	8(%rsp)\t"<<endl;
					cout<<"\tfldl	(%rsp)\t# first operand -> %st(0) ; second operand -> %st(1)"<<endl;
					cout<<"\tfaddp	%st(0),%st(1)\t# %st(0) <- op1 + op2 ; %st(1)=null"<<endl;
					cout<<"\tfstpl 8(%rsp)"<<endl;
					cout<<"\taddq	$8, %rsp"<<endl;
				}
				break;			
			case SUB:
				if(type2!=INTEGER && type2!=DOUBLE) {
					Error("type INTEGER attendu");
				}	
				if(type2==INTEGER) {
					cout << "\tpop %rbx"<<endl;
					cout << "\tpop %rax"<<endl;
					cout << "\tsubq	%rbx, %rax\t# SUB"<<endl;	// add both operands
					cout << "\tpush %rax" << endl;
				}
				else {
					cout<<"\tfldl	(%rsp)"<<endl;
					cout<<"\tfldl	8(%rsp)\t"<<endl;
					cout<<"\tfsubp	%st(0),%st(1)"<<endl;
					cout<<"\tfstpl 8(%rsp)"<<endl;
					cout<<"\taddq	$8, %rsp"<<endl;
				}
				break;
			default:
				Error("opérateur additif inconnu");
		}
	}
	return type1;

}

// DeclarationPart := "[" Ident {"," Ident} "]"
void DeclarationPart(void){
	if(current!=RBRACKET)
		Error("caractère '[' attendu");
	cout << "\t.data"<<endl;
	cout << "\t.align 8"<<endl;
	
	current=(TOKEN) lexer->yylex();
	if(current!=ID)
		Error("Un identificater était attendu");
	cout << lexer->YYText() << ":\t.quad 0"<<endl;
	DeclaredVariables[lexer->YYText()]=INTEGER;
	current=(TOKEN) lexer->yylex();
	while(current==COMMA){
		current=(TOKEN) lexer->yylex();
		if(current!=ID)
			Error("Un identificateur était attendu");
		cout << lexer->YYText() << ":\t.quad 0"<<endl;
		DeclaredVariables[lexer->YYText()]=INTEGER;
		current=(TOKEN) lexer->yylex();
	}
	if(current!=LBRACKET)
		Error("caractère ']' attendu");
	current=(TOKEN) lexer->yylex();
}

// RelationalOperator := "==" | "!=" | "<" | ">" | "<=" | ">="  
OPREL RelationalOperator(void){
	OPREL oprel;
	if(strcmp(lexer->YYText(),"==")==0)
		oprel=EQU;
	else if(strcmp(lexer->YYText(),"!=")==0)
		oprel=DIFF;
	else if(strcmp(lexer->YYText(),"<")==0)
		oprel=INF;
	else if(strcmp(lexer->YYText(),">")==0)
		oprel=SUP;
	else if(strcmp(lexer->YYText(),"<=")==0)
		oprel=INFE;
	else if(strcmp(lexer->YYText(),">=")==0)
		oprel=SUPE;
	else oprel=WTFR;
	current=(TOKEN) lexer->yylex();
	return oprel;
}

// Expression := SimpleExpression [RelationalOperator SimpleExpression]
enum TYPES Expression(void){
	TYPES type1, type2;
	unsigned long long tag;
	OPREL oprel;
	type1=SimpleExpression();
	if(current==RELOP){
		tag=++TagNumber;
		oprel=RelationalOperator();
		type2=SimpleExpression();
		if(type2!=type1) {
			Error("types différents pour la comparaison");
		}
		if(type1!=DOUBLE) {
			cout << "\tpop %rax"<<endl;
			cout << "\tpop %rbx"<<endl;
			cout << "\tcmpq %rax, %rbx"<<endl;
		}
		else {
			cout << "\tfldl (%rsp)\t" << endl;
			cout << "\tfldl 8(%rsp)\t" << endl;
			cout << "\taddq $16, %rsp\t" << endl;
			cout << "\tfcomip %st(1)\t" << endl;
			cout << "\tfaddp %st(1)\t" << endl;
		}
		switch(oprel){
			case EQU:
				cout << "\tje Vrai"<<tag<<"\t# If equal"<<endl;
				break;
			case DIFF:
				cout << "\tjne Vrai"<<tag<<"\t# If different"<<endl;
				break;
			case SUPE:
				cout << "\tjae Vrai"<<tag<<"\t# If above or equal"<<endl;
				break;
			case INFE:
				cout << "\tjbe Vrai"<<tag<<"\t# If below or equal"<<endl;
				break;
			case INF:
				cout << "\tjb Vrai"<<tag<<"\t# If below"<<endl;
				break;
			case SUP:
				cout << "\tja Vrai"<<tag<<"\t# If above"<<endl;
				break;
			default:
				Error("Opérateur de comparaison inconnu");
		}
		cout << "\tpush $0\t\t# False"<<endl;
		cout << "\tjmp Suite"<<tag<<endl;
		cout << "Vrai"<<tag<<":\tpush $0xFFFFFFFFFFFFFFFF\t\t# True"<<endl;	
		cout << "Suite"<<tag<<":"<<endl;
		return BOOL;
	}
	return type1;
}

// AssignementStatement := Identifier ":=" Expression
void AssignementStatement(void){
	TYPES type1, type2;
	string variable;
	if(current!=ID)
		Error("Identificateur attendu");
	if(!IsDeclared(lexer->YYText())){
		cerr << "Erreur : Variable '"<<lexer->YYText()<<"' non déclarée"<<endl;
		exit(-1);
	}
	variable=lexer->YYText();
	type1=DeclaredVariables[variable];
	current=(TOKEN) lexer->yylex();
	if(current!=ASSIGN)
		Error("caractères ':=' attendus");
	current=(TOKEN) lexer->yylex();
	type2=Expression();
	if(type2!=type1) {
		cerr<<"Type variable "<<type1<<endl;
		cerr<<"Type Expression "<<type2<<endl;
		Error("types différents pour l'affectation");
	}
	if(type1==CHAR) {
		cout << "\tpop %rax" << endl;
		cout << "\tmovb %al," << variable << endl;
	}
	else {
		cout << "\tpop "<<variable<<endl;
	}
}

// Statement := AssignementStatement | IfStatement | WhileStatement | ForStatement | BlockStatement
void Statement(void){
	if (current == ID)
	{
		AssignementStatement();
	}
	else if(current==KEYWORD)
	{
		if(!strcmp(lexer->YYText(),"WHILE"))
		{
			WhileStatement();
		}
		else if(!strcmp(lexer->YYText(),"CASE"))
		{
			CaseStatement();
		}
		else if(!strcmp(lexer->YYText(),"IF"))
		{
			IfStatement();
		}
		else if(!strcmp(lexer->YYText(),"FOR"))
		{
			ForStatement();
		}
		else if(!strcmp(lexer->YYText(),"BEGIN"))
		{
			BlockStatement();
		}
		else if(!strcmp(lexer->YYText(),"DISPLAY")) {
			DisplayAssignement();
		}
		else
		{
			Error("Mot clé 'WHILE', 'CASE', 'IF', 'FOR' ou 'BEGIN' attendu");
		}
	}
	else Error("Mot clé ou Identificateur attendu");
}
//Type := TYPES
enum TYPES Type(void) {
	if(current!=KEYWORD) {
		Error("type attendu");
	}
	if(!strcmp(lexer->YYText(),"INTEGER")) {
		current=(TOKEN) lexer->yylex();
		return INTEGER;
	}
	else if(!strcmp(lexer->YYText(),"BOOL")) {
		current=(TOKEN) lexer->yylex();
		return BOOL;
	}
	else if(!strcmp(lexer->YYText(),"CHAR")) {
		current=(TOKEN) lexer->yylex();
		return CHAR;
	}
	else if(!strcmp(lexer->YYText(),"DOUBLE")) {
		current=(TOKEN) lexer->yylex();
		return DOUBLE;
	}
	else {
		Error("types inconnu");
	}
}

//VarDeclaration := ID {"," ID} ":" Type
void VarDeclaration(void) {
	set<string> ident;
	TYPES type;
	if(current!=ID) {
		Error("ID attendu");
	}
	ident.insert(lexer->YYText());
	current=(TOKEN) lexer->yylex();
	while(current==COMMA) {
		current=(TOKEN) lexer->yylex();
		if(current!=ID) {
			Error("ID attendu");
		}
		ident.insert(lexer->YYText());
		current=(TOKEN) lexer->yylex();
	}
	if(current!=COLON) {
		Error("':' attendu");
	}
	current=(TOKEN) lexer->yylex();
	type=Type();
	for(set<string>::iterator it=ident.begin(); it!=ident.end();++it) {
		if(type==INTEGER) {
			cout << *it << ":\t.quad 0" << endl;
		}
		else if(type==BOOL) {
			cout << *it << ":\t" << endl;
		}
		else if(type==DOUBLE) {
			cout << *it << ":\t.double 0.0" << endl;
		}
		else if(type==CHAR) {
			cout << *it << ":\t.byte 0" << endl;
		}
		else {
			Error("type inconnu");
		}
		DeclaredVariables[*it]=type;
	}
	
}
// Constant := NUMBER | CHARCONST
enum TYPES Constant(void) {
	enum TYPES type;
	if(current==NUMBER) {
		type=Number();
	}
	else if(current==CHARCONST) {
		type = ConstChar();
	}
	else {
		Error("Nombre ou Char attendu");
	}
	return type;
}
// VarDeclarationPart := "VAR" VarDeclaration {";" VarDeclaration} "."
void VarDeclarationPart(void) {
	current=(TOKEN) lexer->yylex();
	VarDeclaration();
	while(current==SEMICOLON) {
		current=(TOKEN) lexer->yylex();
		VarDeclaration();
	}
	if(current!=DOT) {
		Error("'.' était attendu après la déclaration de variable");
	}
	current=(TOKEN) lexer->yylex();
}

// StatementPart := Statement {";" Statement} "."
void StatementPart(void){
	cout << "\t.align 8" << endl;
	cout << "\t.text\t\t# The following lines contain the program"<<endl;
	cout << "\t.globl main\t# The main function must be visible from outside"<<endl;
	cout << "main:\t\t\t# The main function body :"<<endl;
	cout << "\tmovq %rsp, %rbp\t# Save the position of the stack's top"<<endl;
	Statement();
	while(current==SEMICOLON){
		current=(TOKEN) lexer->yylex();
		Statement();
	}
	if(current!=DOT)
		Error("caractère '.' attendu");
	current=(TOKEN) lexer->yylex();
}

// Program := [DeclarationPart] StatementPart
void Program(void){
	if(current==RBRACKET) {
		DeclarationPart();
	}
	else if(current==KEYWORD && !strcmp(lexer->YYText(),"VAR")) {
		VarDeclarationPart();
	}
	StatementPart();	
}

int main(void){	// First version : Source code on standard input and assembly code on standard output
	// Header for gcc assembler / linker
	cout << "\t\t\t# This code was produced by COLIN Theo"<<endl;
	cout << ".data"<<endl;
	cout << "FormatString1:\t.string \"%llu\"\t# used by printf to display 64-bit unsigned integers"<<endl; 
	cout << "FormatString2:\t.string \"%lf\"\t# used by printf to display 64-bit floating point numbers"<<endl; 
	cout << "FormatString3:\t.string \"%c\"\t# used by printf to display a 8-bit single character"<<endl; 
	cout << "TrueString:\t.string \"TRUE\"\t# used by printf to display the boolean value TRUE"<<endl; 
	cout << "FalseString:\t.string \"FALSE\"\t# used by printf to display the boolean value FALSE"<<endl;
	// Let's proceed to the analysis and code production
	current=(TOKEN) lexer->yylex();
	Program();
	// Trailer for the gcc assembler / linker
	cout << "\tmovq %rbp, %rsp\t\t# Restore the position of the stack's top"<<endl;
	cout << "\tret\t\t\t# Return from main function"<<endl;
	if(current!=FEOF){
		cerr <<"Caractères en trop à la fin du programme : ["<<current<<"]";
		Error("."); // unexpected characters at the end of program
	}

}

